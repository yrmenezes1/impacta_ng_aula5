echo "Instalando Kubectl"
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl

chmod +x kubectl

mv kubectl  /usr/local/bin/

kubectl version --client -o json

echo "Instalando Minikube"
wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
chmod +x minikube-linux-amd64
mv minikube-linux-amd64 /usr/local/bin/minikube
minikube version

echo "Instalando Podman"
dnf install podman -y
systemctl start podman
systemctl enable podman

echo "Inciando minikube"
minikube start --force

